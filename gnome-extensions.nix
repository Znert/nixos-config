{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    gnomeExtensions.blur-my-shell
    gnomeExtensions.dash-to-dock
    gnomeExtensions.middle-click-to-close-in-overview
    gnomeExtensions.gsconnect
  ];

  # Needed for gsconnect
  networking.firewall = {
    allowedUDPPortRanges = [
      { from = 1716; to = 1764; }
    ];
    allowedTCPPortRanges = [
      { from = 1716; to = 1764; }
    ];
  };
}
