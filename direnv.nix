{ pkgs, ...}:

{
  # Enable nix-direnv
  programs.direnv.enable = true;
}