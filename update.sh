#!/usr/bin/env bash

echo
echo "Updating flake..."
echo

nix flake update

echo
echo "Flake updated"
echo

echo
echo "Commiting changes"
echo

git add flake.lock
git commit -m "update flake.lock"

echo
echo "Updating OS"
echo

sudo nixos-rebuild switch --flake .

echo
echo "Pushing Git changes"
echo

git push