{ config, pkgs, ... }:

{
  imports = [
    ../base-configuration.nix
    ./hardware-configuration.nix
    ../gnome-extensions.nix
    ../web-dev.nix
    ../app-image.nix
    ../direnv.nix
    ../modules/games.nix
    ../modules/nix-ld.nix
    ../modules/shell/zsh.nix
  ];

  environment.systemPackages = [
    (import ../packages/prime-run.nix { inherit pkgs; })
  ];

  boot.plymouth.enable = true;
  boot.initrd.systemd.enable = true;
  boot.kernelParams = [ "quiet" ];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.znert = {
    isNormalUser = true;
    description = "Simon Hayden";
    extraGroups = [ "networkmanager" "wheel"  "adbusers" ];
    shell = pkgs.zsh;
  };

  services.teamviewer.enable = true;

  # Enable openrazer drivers
  hardware.openrazer.enable = true;
  hardware.openrazer.users = ["znert"];

  # Enable OpenGL
  hardware.graphics = {
    enable = true;
  };

  # Load nvidia driver for Xorg and Wayland
  services.xserver.videoDrivers = ["nvidia"];

  hardware.nvidia = {
    # Use open source driver?
    open = false;

    # Modesetting is required.
    modesetting.enable = true;

    # Nvidia power management. Experimental.
    powerManagement.enable = false;

    # Enable the Nvidia settings menu,
	  # accessible via `nvidia-settings`.
    nvidiaSettings = true;

    # Optionally, you may need to select the appropriate driver version for your specific GPU.
    package = config.boot.kernelPackages.nvidiaPackages.stable;

    prime = {
      sync.enable = true;
      # Extracted using "sudo lshw -c display"
      intelBusId = "PCI:0:2:0";
      nvidiaBusId = "PCI:1:0:0";
    };
  };

  programs.adb.enable = true;


  services.switcherooControl.enable = true;
}
