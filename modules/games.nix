{ pkgs, ...}:

{
  # Install + enable Steam
  programs.steam.enable = true;
}